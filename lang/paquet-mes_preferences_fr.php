<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'mes_preferences_nom' => 'Mes pr&eacute;f&eacute;rences',
	'mes_preferences_description' => 'Ce plugin modifie le système des préférences utilisateur de spip pour lui ajouter des fonctionalités ou des options supplémentaires',
	'mes_preferences_slogan' => 'Ajouter des préférences utilisateur'
);
